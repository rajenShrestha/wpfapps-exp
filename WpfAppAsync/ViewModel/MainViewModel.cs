﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using WpfAppAsync.Model;

namespace WpfAppAsync.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged(WelcomeTitlePropertyName);
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });

#if DEBUG
            if (IsInDesignMode)
            {
                WebContent = "Hello mate you have to get content from a web site";
            }
#endif
        }

        /// <summary>
        /// The <see cref="WebContent" /> property's name.
        /// </summary>
        public const string WebContentPropertyName = "WebContent";

        private string _webContent;

        /// <summary>
        /// Sets and gets the WebContent property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WebContent
        {
            get
            {
                return _webContent;
            }
            set
            {
                Set(WebContentPropertyName, ref _webContent, value);
                GetWebContentCommand.RaiseCanExecuteChanged();
            }
        }

        private GalaSoft.MvvmLight.Command.RelayCommand _getWebContent;

        /// <summary>
        /// Gets the GetWebContent.
        /// </summary>
        public GalaSoft.MvvmLight.Command.RelayCommand GetWebContent
        {
            get
            {
                return _getWebContent
                    ?? (_getWebContent = new GalaSoft.MvvmLight.Command.RelayCommand(
                    async () =>
                    {
                        WebContent = string.Empty;
                        var httpClient = new HttpClient();
                        Task<string> content = httpClient.GetStringAsync("http://msdn.microsoft.com");
                        WebContent = await content;
                    }));
            }
        }

        private RelayCommand<string> _getWebContentCommand;

        /// <summary>
        /// Gets the GetWebContentCommand.
        /// </summary>
        public RelayCommand<string> GetWebContentCommand
        {
            get
            {
                return _getWebContentCommand
                    ?? (_getWebContentCommand = new RelayCommand<string>(
                        url => SetContent(url)));
            }
        }

        private async void SetContent(string url)
        {
            WebContent = string.Empty;
            var httpClient = new HttpClient();
            Task<string> content = httpClient.GetStringAsync(url);
            WebContent = await content;
        }

        #region TPL Example
        /// <summary>
        /// The <see cref="Result" /> property's name.
        /// </summary>
        public const string ResultPropertyName = "Result";

        private string _result;

        /// <summary>
        /// Sets and gets the Result property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Result
        {
            get
            {
                return _result;
            }

            set
            {
                if (_result == value)
                {
                    return;
                }

                _result = value;
                RaisePropertyChanged(() => Result);
            }
        }

        private RelayCommand _startTaskCommand;

        /// <summary>
        /// Gets the StartTaskComman.
        /// </summary>
        public RelayCommand StartTaskComman
        {
            get
            {
                return _startTaskCommand
                    ?? (_startTaskCommand = new RelayCommand(
                    () =>
                    {
                        Task<string> resultTask = new Task<string>(() =>
                        {
                            int value = 0;
                            for (int index = 0; index < 10; index++)
                            {
                                Thread.Sleep(500);
                                value = index + 1;
                            }
                            return "Result is " + value ;
                        });
                        resultTask.Start();
                        
                        resultTask.ContinueWith(rt => {
                            Result = rt.Result;
                        });
                    }));
            }
        }
        #endregion
        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}