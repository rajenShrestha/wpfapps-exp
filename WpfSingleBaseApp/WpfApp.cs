﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfSingleBaseApp
{
    public partial class WpfApp: Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();

            if (e.Args.Length > 0)
            {
                ShowDocument(e.Args[0]);
            }
        }

        private void ShowDocument(string file)
        {
            
        }
    }
}
