﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSingleBaseApp
{
    public class Startup
    {
        [STAThread]
        public static void Main(string[] args)
        {
            WpfSingleBaseApp.Starters.SingleInstanceApplicationWrapper wrapper = new Starters.SingleInstanceApplicationWrapper();
            wrapper.Run(args);
        }
    }
}
