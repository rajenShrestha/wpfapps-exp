﻿using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wf = System.Windows;

namespace WpfSingleBaseApp.Starters
{
    public class SingleInstanceApplicationWrapper : WindowsFormsApplicationBase
    {
        private WpfApp mainApplication;
        public SingleInstanceApplicationWrapper()
        {
            IsSingleInstance = true;
        }

        protected override bool OnStartup(StartupEventArgs eventArgs)
        {
            mainApplication = new WpfApp();
            mainApplication.Run();
            return false;
        }

        protected override void OnStartupNextInstance(StartupNextInstanceEventArgs eventArgs)
        {
            Wf.MessageBox.Show("hello, not implemented yet");
            if (eventArgs.CommandLine.Count > 0)
            {

            }
        }
    }
}
