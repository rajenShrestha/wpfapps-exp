﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WpfSingleBaseApp
{
    internal static class ResourceHelper
    {
        internal static BitmapImage GetImage(string imageName)
        {
            return new BitmapImage(new Uri("Images/" + imageName, UriKind.Relative));
        }

        /// <summary>
        /// Name of images that are added in images folder 
        /// Images are added as resource in build action
        /// </summary>
        /// <returns></returns>
        internal static List<string> GetImageName()
        {
            List<string> imageNameList = new List<string>();
            imageNameList.Add("design Template-1.PNG");
            imageNameList.Add("design Template.PNG");
            imageNameList.Add("oo languages.PNG");
            imageNameList.Add("SOLID-OODesign-Principle.PNG");
            return imageNameList;
        }
    }
}
