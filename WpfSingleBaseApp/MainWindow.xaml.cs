﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfSingleBaseApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> namesList;
        private int currentImageIndex = 0;

        public MainWindow()
        {
            InitializeComponent();
            namesList = ResourceHelper.GetImageName();
            imageView.Source = ResourceHelper.GetImage(namesList[currentImageIndex]);
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            if (currentImageIndex == namesList.Count)
            {
                return;
            }
            imageView.Source = ResourceHelper.GetImage(namesList[currentImageIndex + 1]);
        }

        private void buttonPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (currentImageIndex == 0)
            {
                return;
            }
            imageView.Source = ResourceHelper.GetImage(namesList[currentImageIndex - 1]);
            
        }
    }
}
