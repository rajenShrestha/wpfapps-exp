﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfAppShapeWindow.Common.Repository;
using WpfAppShapeWindow.Common.ViewModel;

namespace WpfAppShapeWindow
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            IUserRepository userRepository = new DemoUserRepository();
            var mainViewModel = new MainWindowViewModel(userRepository);
            var userViewModel = new UserViewModel(userRepository);

            var mainWindow = new MainWindow();
            mainWindow.mainGrid.DataContext = mainViewModel.Users;
            //mainWindow.userDetail.DataContext = userViewModel;
            mainWindow.Show();
        }


    }
}
