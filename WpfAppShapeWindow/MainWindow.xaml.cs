﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppShapeWindow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            borderList.MouseLeftButtonDown += MainW_MouseLeftButtonDown;
            borderPic.MouseLeftButtonDown += MainW_MouseLeftButtonDown;
            borderDescription.MouseLeftButtonDown += MainW_MouseLeftButtonDown;

            buttonClose.Click += (sender, e) => {
                this.Close();
            };
            buttonMinimize.Click += (sender, e) => {
                WindowState = System.Windows.WindowState.Minimized;
            };
        }
        
        private void MainW_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

       

    }
}
