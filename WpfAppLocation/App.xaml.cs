﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace WpfAppLocation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            if (e.Args.Length > 0)
            {
                List<string> arguments = e.Args.ToList();
                foreach (string arg in arguments)
                {
                    MessageBox.Show("Message you have passed is" + arg, "Wpf Location");
                }
            }
        }
    }
}
