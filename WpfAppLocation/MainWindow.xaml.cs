﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppLocation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Remembers the last location of this window
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            if (Properties.Settings.Default.WindowsPosition.Location.X != 0.0)
            {
                Point _windowsLocation = Properties.Settings.Default.WindowsPosition.Location;
                PositionWindowsLocation(_windowsLocation.X, _windowsLocation.Y);
            }
            else
            {
                this.PositionWindowsAtBottom();
            }

        }

        private void PositionWindowsAtBottom()
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
        }

        private void PositionWindowsLocation(double top, double left)
        {
            this.Top = top;
            this.Left = left;
        }

        protected override void OnClosed(EventArgs e)
        {
            Rect windowsPosition = Properties.Settings.Default.WindowsPosition;
            windowsPosition.Location = new Point(this.Top, this.Left);

            Properties.Settings.Default.WindowsPosition = windowsPosition;
            Properties.Settings.Default.Save();
            base.OnClosed(e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IOperation operation = (IOperation)Application.Current;
            operation.Show(typeof(AboutWindow).ToString(), this);
        }
    }
}
