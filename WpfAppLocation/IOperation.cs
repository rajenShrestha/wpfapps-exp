﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace WpfAppLocation
{
    internal interface IOperation
    {
        void Show(string nameWindow, Window parent);
    }
}
