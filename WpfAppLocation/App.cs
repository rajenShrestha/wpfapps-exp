﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfAppLocation
{
    /// <summary>
    /// This is partial class specific to this appliaction
    /// </summary>
    public partial class App : Application, IOperation
    {
        public void ShowMessage(string message)
        {
            MessageBox.Show(message, "Application");
        }

        public void Show(string nameWindow, Window parent)
        {
            if (nameWindow == typeof(AboutWindow).ToString())
            {
                AboutWindow window = new AboutWindow();
                window.Owner = parent;
                window.ShowDialog();
            }
        }
    }
}
