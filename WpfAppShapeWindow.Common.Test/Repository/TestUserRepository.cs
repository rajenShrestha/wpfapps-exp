﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfAppShapeWindow.Common.Models;
using WpfAppShapeWindow.Common.Repository;

namespace WpfAppShapeWindow.Common.Test.Repository
{
    [TestFixture]
    public class TestUserRepository
    {
        [Test]
        public void TestGetById()
        {
            DemoUserRepository rep = new DemoUserRepository();
            User u = rep.Get(1);
            Assert.AreEqual(u.Name, "Rajen");
        }

        [Test]
        public void TestGetByIdNonExist()
        {
            DemoUserRepository rep = new DemoUserRepository();
            User u = rep.Get(5);
            Assert.AreEqual(u.Name, null);
        }
    }
}
