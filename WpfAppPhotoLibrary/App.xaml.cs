﻿using Au.RS.WpfAppPhotoLibrary.Common.Models;
using Au.RS.WpfAppPhotoLibrary.Common.Repository;
using Au.RS.WpfAppPhotoLibrary.Common.Services;
using Au.RS.WpfAppPhotoLibrary.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace WpfAppPhotoLibrary
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
        }
    }
}
