﻿using Au.RS.WpfAppPhotoLibrary.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppPhotoLibrary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            AddHotKeys();
        }

        private void AddHotKeys()
        {
            try
            {
                IHotKeyCommand hotKeyNextCommand = new HotKeyNextCommand((IWindowOperationService)Application.Current);
                CommandBindings.Add(hotKeyNextCommand.Create());

                IHotKeyCommand openFolderBrowserCommand = new OpenFolderCommand((IWindowOperationService)Application.Current);
                CommandBindings.Add(openFolderBrowserCommand.Create());
            }
            catch (Exception)
            {
                //handle exception error
            }
        }
    }


}
