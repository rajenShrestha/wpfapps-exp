﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
*/
using Au.RS.WpfAppPhotoLibrary.Common.Models;
using Au.RS.WpfAppPhotoLibrary.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace WpfAppPhotoLibrary
{
    public partial class App: IWindowOperationService
    {
        public string ShowSourceFolderBrowser()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.SelectedPath = @"E:\Pictures";
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return folderBrowserDialog.SelectedPath;
            }
            return string.Empty;
        }

        public void ShowSelectedPhoto(Photo photo)
        {
            var photoView = new PhotoView();
            photoView.DataContext = photo;
            photoView.ShowDialog();
        }
    }
}