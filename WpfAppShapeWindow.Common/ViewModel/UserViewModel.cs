﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfAppShapeWindow.Common.Models;
using WpfAppShapeWindow.Common.Repository;

namespace WpfAppShapeWindow.Common.ViewModel
{
    public class UserViewModel : ViewModelBase
    {
        private IUserRepository userRepository;

        public UserViewModel(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
               
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string middleName;

        public string MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }
        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        private string comment;

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }
        private string imageUri;

        public string ImageUri
        {
            get { return imageUri; }
            set { imageUri = value; }
        }


    }
}
