﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfAppShapeWindow.Common.Models;

namespace WpfAppShapeWindow.Common.Repository
{
    public interface IUserRepository
    {
        IEnumerable<User> Get();
        void Save(User user);
        User Get(int id);
    }
}
