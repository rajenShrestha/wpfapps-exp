﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfAppShapeWindow.Common.Models;

namespace WpfAppShapeWindow.Common.Repository
{
    public class DemoUserRepository : IUserRepository
    {
        private List<User> users = new List<User>();
        public DemoUserRepository()
        {
            SetUp();
        }
        public IEnumerable<User> Get()
        {
            return users;
        }

        public void Save(User user)
        {
            users.Add(user);
        }

        public User Get(int id)
        {
            User u = new User();
            var user = users.Where(p => p.Id == id);
            int i = user.Count();
            if (i > 0)
            {
                u = user.First();
            }
            return u;
        }

        public void SetUp()
        {
            User a = new User()
            {
                Id = 1,
                Comment = "This is first user",
                ImageUri = @"C:\temp\Images\Alien.png",
                LastName = "Shrestha",
                Name = "Rajen"
            };

            User b = new User()
            {
                Id = 2,
                Comment = "This is second user",
                ImageUri = @"C:\temp\Images\user.png",
                LastName = "Shrestha",
                Name = "Zuzana"
            };

            User c = new User()
            {
                Id = 1,
                Comment = "This is third user",
                ImageUri = @"C:\temp\Images\user.png",
                LastName = "Shrestha",
                Name = "Krish"
            };

            users.Add(a);
            users.Add(b);
            users.Add(c);
        }
    }
}
