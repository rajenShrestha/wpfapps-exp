﻿using Au.RS.WpfAppPhotoLibrary.Common.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppPhotoLibrary.Common.Test.Utils
{
    [TestFixture]
    public class FolderRepositoryHelperTest
    {
        [Test]
        public void TestCreateFilterString()
        {
            string extensions = "jpg,jpeg,png";
            string expectedFilter = "*.jpg|*.jpeg|*.png";

            string value = FolderPhotoRepositoryHelper.CreateFilterString(extensions);
            Assert.AreEqual(expectedFilter, value);
        }

        [Test]
        public void TestCreateFilterStringEmptyExtension()
        {
            string extensions = "";
            string expectedFilter = "";

            string value = FolderPhotoRepositoryHelper.CreateFilterString(extensions);
            Assert.AreEqual(expectedFilter, value);
        }        
    }
}
