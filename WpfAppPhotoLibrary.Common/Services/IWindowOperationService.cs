﻿using Au.RS.WpfAppPhotoLibrary.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Au.RS.WpfAppPhotoLibrary.Common.Services
{
    public interface IWindowOperationService
    {
        string ShowSourceFolderBrowser();
        void ShowSelectedPhoto(Photo photo);
    }
}
