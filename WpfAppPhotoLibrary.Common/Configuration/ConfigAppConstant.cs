﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Au.RS.WpfAppPhotoLibrary.Common.Configuration
{
    public static class ConfigAppConstant
    {
        public static readonly string APPSETTING_PHOTO_FOLDER_PATH = "initialPhotoFolderPath";
        public static readonly string APPSETTING_FILE_EXTENSIONS = "supportedFileExtensions";
    }
}
