﻿using Au.RS.WpfAppPhotoLibrary.Common.Configuration;
using Au.RS.WpfAppPhotoLibrary.Common.Services;
using Au.RS.WpfAppPhotoLibrary.Common.Repository;
using Au.RS.WpfAppPhotoLibrary.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Au.RS.WpfAppPhotoLibrary.Common.Locator
{
    public class ViewModelLocator
    {
        private IPhotoRepository _photoRepository;
        private IWindowOperationService _operationService;

        public static ViewModelLocator Instance
        {
            get
            {
                return (ViewModelLocator)Application.Current.Resources["viewModelLocator"];
            }
        }

        public MainViewModel MainViewModel
        {
            get;
            private set;
        }

        public ViewModelLocator()
        {
            string folderPath = ConfigurationManager.AppSettings[ConfigAppConstant.APPSETTING_PHOTO_FOLDER_PATH];
            string fileExtensions = ConfigurationManager.AppSettings[ConfigAppConstant.APPSETTING_FILE_EXTENSIONS];

            _photoRepository = new FolderPhotoRepository(folderPath, fileExtensions);
            _operationService = (IWindowOperationService)Application.Current;
            MainViewModel = new MainViewModel(_photoRepository, _operationService);
        }
    }
}
