﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Au.RS.WpfAppPhotoLibrary.Common.Utils
{
    public static class FolderPhotoRepositoryHelper
    {
        /// <summary>
        /// Create Filter string for example "*.gif|*.jpg|*.png|*.bmp|*.aspx|*.vb" from the given comma separated parameter
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string CreateFilterString(string extension)
        {
            string value = string.Empty;
            if (string.IsNullOrEmpty(extension))
            {
                return value;
            }

            extension.Split(',').ToList<string>().ForEach(delegate(string e)
            {
                value = value + "*." + e + "|";
            });
            return value.Remove(value.Length - 1);
        }

        public static IList<string> GetFileExtensions(string extension)
        {
            return extension.Split(',').ToList();
        }
    }
}
