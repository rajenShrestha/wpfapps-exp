﻿using Au.RS.WpfAppPhotoLibrary.Common.Services;
using Au.RS.WpfAppPhotoLibrary.Common.Models;
using Au.RS.WpfAppPhotoLibrary.Common.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight.CommandWpf;

namespace Au.RS.WpfAppPhotoLibrary.Common.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private List<Photo> photos;
        private Photo selectedPhoto;
        private string progressMessage;

        private IPhotoRepository _photoRepository;
        private IWindowOperationService _operationService;

        private string _selectedFolder;

        //this belong to this class because it will be connected to ui
        private bool _progressVisiblity;


        public MainViewModel(IPhotoRepository photoRepository, IWindowOperationService operationService)
        {
            _photoRepository = photoRepository;
            _operationService = operationService;
            ProgressVisiblity = false;
        }

        public List<Photo> Photos
        {
            get { return photos; }
            set
            {
                photos = value;
                OnPropertyChanged();
            }
        }

        public Photo SelectedPhoto
        {
            get { return selectedPhoto; }
            set
            {
                selectedPhoto = value;
               
            }
        }

        public string ProgressMessage
        {
            get { return progressMessage; }
            set
            {
                progressMessage = value;
                OnPropertyChanged();
            }
        }

        public bool ProgressVisiblity
        {
            get { return _progressVisiblity; }
            set
            {
                _progressVisiblity = value;
                OnPropertyChanged();
            }
        }

        //command that will be connected to a button
        private RouteCommand _loadPhotoFromFolderCommand;
        public RouteCommand LoadPhotoFromFolderCommand
        {
            get
            {
                if (this._loadPhotoFromFolderCommand == null)
                {
                    this._loadPhotoFromFolderCommand = new RouteCommand(p => this.LoadPhotoFormFolder(""));
                }
                return _loadPhotoFromFolderCommand;
            }
        }

        private void LoadPhotoFormFolder(string folderPath, bool showFolderBrowser = true)
        {
            if (showFolderBrowser)
            {
                _selectedFolder = _operationService.ShowSourceFolderBrowser();
            }
            else
            {
                _selectedFolder = folderPath;
            }
            this.RetrievePhotoAsync();
        }

        private RouteCommand _stopProgressRetrieveCommand;
        public RouteCommand StopProgressRetrieveCommand
        {
            get
            {
                if (this._stopProgressRetrieveCommand == null)
                {
                    this._stopProgressRetrieveCommand = new RouteCommand(p =>
                    {                       
                    });
                } return _stopProgressRetrieveCommand;
            }
        }

        private RelayCommand<Photo> _showSelectedPhotoCommand;

        /// <summary>
        /// Gets the ShowSelectedPhotoCommand.
        /// </summary>
        public RelayCommand<Photo> ShowSelectedPhotoCommand
        {
            get
            {
                return _showSelectedPhotoCommand
                    ?? (_showSelectedPhotoCommand = new RelayCommand<Photo>(
                    photo =>
                    {
                        _operationService.ShowSelectedPhoto(photo);
                    }));
            }
        }

        protected async void RetrievePhotoAsync()
        {
            ClearMemory();
            //clear list view
            Photos = new List<Photo>();
            Thread.Sleep(500);

            //show progress bar
            ProgressVisiblity = true;

            var taskGetPhoto = new Task<List<Photo>>(() =>
            {
                List<Photo> pl = new List<Photo>();
                _photoRepository.UpdateProgessEvent += (count =>
                {
                    CurrentProgress = count;
                });

                //this event will be called when a photo object is creted on photoRepository
                _photoRepository.GetPhotoEvent += (p =>
                {
                    pl.Add(p);
                    ProgressMessage = p.Path.LocalPath;
                });
                _photoRepository.Get(_selectedFolder);
                return pl;
            });
            taskGetPhoto.Start();
            Photos = await taskGetPhoto;
            ProgressVisiblity = false;
        }

        private void ClearMemory()
        {
            if (photos == null)
            {
                return;
            }
            photos.ForEach(delegate(Photo p)
            {
                p.Dispose();
            });
        }
    }
}
