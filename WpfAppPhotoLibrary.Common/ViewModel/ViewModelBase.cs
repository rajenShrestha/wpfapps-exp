﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Au.RS.WpfAppPhotoLibrary.Common.ViewModel
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        private double _currentProgress;               
        public event PropertyChangedEventHandler PropertyChanged;

        public ViewModelBase()
        {           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void RetrievePhoto(object sender, DoWorkEventArgs e)
        {
            //implement on child object if required
        }

        public double CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                if (_currentProgress != value)
                {
                    _currentProgress = value;
                    OnPropertyChanged();
                }
            }
        }

        public void OnPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
