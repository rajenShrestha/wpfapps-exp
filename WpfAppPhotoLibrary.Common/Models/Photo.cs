﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Au.RS.WpfAppPhotoLibrary.Common.Models
{
    public class Photo : IDisposable
    {
        public Photo()
        {

        }

        private BitmapFrame _image;

        public BitmapFrame Image
        {
            get { return _image; }
            set { _image = value; }
        }

        private Uri _path;

        public Uri Path
        {
            get { return _path; }
            set { _path = value; }
        }

        private Metadata _metadata;

        public Metadata Metadata
        {
            get { return _metadata; }
            set { _metadata = value; }
        }

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);   
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                _image = null;
                _path = null;
                _metadata = null;
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }

    }
}
