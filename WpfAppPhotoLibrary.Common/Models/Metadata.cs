﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Au.RS.WpfAppPhotoLibrary.Common.Models
{
    public class Metadata
    {
        private string _place;

        public string Place
        {
            get { return _place; }
            set { _place = value; }
        }
        private string _time;

        public string Time
        {
            get { return _time; }
            set { _time = value; }
        }
        private string _day;

        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
