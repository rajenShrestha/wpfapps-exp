﻿using Au.RS.WpfAppPhotoLibrary.Common.Models;
using Au.RS.WpfAppPhotoLibrary.Common.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Au.RS.WpfAppPhotoLibrary.Common.Repository
{

    public class FolderPhotoRepository : IPhotoRepository
    {
        private string _folderPath;
        private string _fileExtension;
        private List<Photo> _photos;

        public event UpdateProgressDelegate UpdateProgessEvent;
        public event GetPhotoDelegate GetPhotoEvent;

        /// <summary>
        /// The fileExtension must contain dot (eg: .jpg)
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="fileExtension"></param>
        public FolderPhotoRepository(string folderPath, string fileExtension)
        {
            _fileExtension = fileExtension;
            _folderPath = folderPath;
        }

        public List<Models.Photo> GetAll()
        {
            _photos = new List<Photo>();
            CreateList(_folderPath);
            return _photos;
        }

        public void Get(string selectPath)
        {
            CreateList(selectPath, false);
        }

        /// <summary>
        /// If addTolist is true, then it adds the photo object to list otherwise it calls
        /// the the GetPhotoEvent
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="addToList"></param>
        private void CreateList(string folderPath, bool addToList = true)
        {
            _photos = new List<Photo>();
            try
            {
                IList<string> fileExtension = FolderPhotoRepositoryHelper.GetFileExtensions(_fileExtension);

                //get all file from folder and filter with where extension method
                IEnumerable<string> files = Directory.EnumerateFiles(folderPath, "*.*", SearchOption.AllDirectories)
                    .Where(f => fileExtension.Any(ext => ext.ToUpper() == Path.GetExtension(f).ToUpper()));

                
                double count = 0;
                double totalFiles = files.Count();
                foreach (string file in files)
                {
                    Photo p = CreatePhoto(file);
                    if (UpdateProgessEvent != null)
                    {
                        double value = ++count / totalFiles;
                        int progressValue = (int)(value * 100);
                        UpdateProgessEvent(progressValue);
                    }
                    if (addToList)
                    {
                        _photos.Add(p);
                    }
                    else
                    {
                        if (GetPhotoEvent != null)
                        {
                            GetPhotoEvent(p);
                        }
                    }
                }
            }
            catch (ArgumentNullException)
            {
                throw;
            }
        }

        private Photo CreatePhoto(string file)
        {
            Photo p = new Photo()
            {
                Path = new Uri(file),
                Image = BitmapFrame.Create(new Uri(file)),
                Metadata = new Metadata()
                {
                    Day = "Unknown",
                    Description = "This is learning phase",
                    Place = "Unknown place",
                    Time = "00:00"
                }
            };
            return p;
        }
    }
}
