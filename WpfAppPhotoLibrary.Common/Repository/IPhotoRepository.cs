﻿using Au.RS.WpfAppPhotoLibrary.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Au.RS.WpfAppPhotoLibrary.Common.Repository
{
    public delegate void UpdateProgressDelegate(int value);
    public delegate void GetPhotoDelegate(Photo photo);

    public interface IPhotoRepository
    {
        List<Photo> GetAll();
        event UpdateProgressDelegate  UpdateProgessEvent;
        event GetPhotoDelegate GetPhotoEvent;
        //Photo will be added by GetPhotoDelegate
       void Get(string selectPath);
    }
}
