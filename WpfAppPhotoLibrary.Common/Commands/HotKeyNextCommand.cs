﻿using Au.RS.WpfAppPhotoLibrary.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Au.RS.WpfAppPhotoLibrary.Common.Services
{
    /// <summary>
    /// Alt + A
    /// </summary>
    public class HotKeyNextCommand : IHotKeyCommand
    {
        private IWindowOperationService _operation;

        /// <summary>
        /// Alt + A
        /// </summary>
        /// <param name="operation"></param>
        public HotKeyNextCommand(IWindowOperationService operation)
        {
            _operation = operation;
        }

        public CommandBinding Create()
        {
            RoutedCommand command = new RoutedCommand();
            command.InputGestures.Add(new KeyGesture(Key.A, ModifierKeys.Alt));
            CommandBinding cb = new CommandBinding(command, DoWork);
            return cb;
        }

        public void DoWork(object sender, ExecutedRoutedEventArgs e)
        {            
        }
    }


}
