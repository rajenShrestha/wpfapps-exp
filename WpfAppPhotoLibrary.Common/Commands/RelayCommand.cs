﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Au.RS.WpfAppPhotoLibrary.Common.Services
{
    public class RouteCommand : System.Windows.Input.ICommand
    {
        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;
        public RouteCommand(Action<object> execute)
            : this(execute, null)
        {
            this.execute = execute;
        }
        public RouteCommand(Action<object> execute, Predicate<object> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null ? true : canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }
}
