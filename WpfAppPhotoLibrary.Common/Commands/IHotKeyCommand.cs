﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Au.RS.WpfAppPhotoLibrary.Common.Services
{
    public interface IHotKeyCommand
    {
        /// <summary>
        /// <example>
        ///     RoutedCommand command = new RoutedCommand();
        ///     command.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
        ///     CommandBinding cb = new CommandBinding(command, DoWork);
        ///     return cb;
        /// </example>
        /// </summary>
        /// <returns></returns>
        CommandBinding Create();

        void DoWork(object sender, ExecutedRoutedEventArgs e);
    }
}
