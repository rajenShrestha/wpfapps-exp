﻿using Au.RS.WpfAppPhotoLibrary.Common.Locator;
using Au.RS.WpfAppPhotoLibrary.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Au.RS.WpfAppPhotoLibrary.Common.Services
{
    public class OpenFolderCommand : IHotKeyCommand
    {
        private IWindowOperationService _operation;
        /// <summary>
        /// Ctrl + O (open folder browser)
        /// </summary>
        /// <param name="operation"></param>
        public OpenFolderCommand(IWindowOperationService operation)
        {
            _operation = operation;
        }

        public System.Windows.Input.CommandBinding Create()
        {
            //TODO: how to do this properly
            RoutedCommand command = new RoutedCommand();
            command.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
            CommandBinding cb = new CommandBinding(command, DoWork);
            return cb;
        }

        public void DoWork(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            MainViewModel vm = ViewModelLocator.Instance.MainViewModel;
            vm.LoadPhotoFromFolderCommand.Execute(null);
        }
    }
}
